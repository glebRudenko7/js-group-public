document.addEventListener('DOMContentLoaded', function() {
	var ball = document.getElementById('ball');
	var ball2 = document.getElementById('ball2');
	var parentHeightBorder = ball.parentNode.clientHeight - ball.clientHeight;
	var parentWidthBorder = ball2.parentNode.clientWidth - ball2.clientWidth;

	function jumpingBall() {
	  if (parseInt(ball.style.bottom) !== 0) {
	  	ballDown();
	  }
	  else {
	  	ballUp();
	  }
	}

	function bouncingBall() {
		if (parseInt(ball2.style.left) !== 0) {
	  	ballLeft();
	  }
	  else {
	  	ballRight();
	  }
	}

	function ballDown() {
	  ball.style.bottom = "0px";
	}

	function ballUp() {
	  ball.style.bottom = parentHeightBorder + "px";
	}

	function ballLeft() {
	  ball2.style.left = "0px";

	}

	function ballRight() {
	  ball2.style.left = parentWidthBorder + "px";
	}

	ball.addEventListener('transitionend', jumpingBall);
	ball2.addEventListener('transitionend', bouncingBall);
	
	// Key events

	document.addEventListener('keyup', function(e) {
	  var key = e.keyCode || e.which;
	  key == 49 && ballDown();
	  key == 50 && ballRight();
	});
});