document.addEventListener('DOMContentLoaded', function() {
	var ball = document.getElementById('ball');
	ball.addEventListener('mousedown', handler);
	document.addEventListener('mouseup', handler2);

	function move(event) {
		ball.style.top = event.pageY + "px";
		ball.style.left = event.pageX + "px";
	}
	function handler(event) {
		document.addEventListener('mousemove', move);
	}

	function handler2(event) {
		document.removeEventListener('mousemove', move);
	}

});