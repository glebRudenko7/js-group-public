$(document).ready(function() {

	$('ul.nav li button').hover(function() {
		$(this).css('background-color', '#00b300');
	}, function() {
		$(this).css('background-color', '#ffcc00');
	});

	$('ul.nav li button').click(function(e) {
		$(this).hide(500).delay(800).show(500);
	});

	$('nav').before('<h2>This is our content</h2>');
	$('nav').after('<p><i>some tet goes here<i></p>');

	$('#content > p').css('font-size', '24px');

	$('#content > p i, #content > p b').css('color', 'blue');

	// Image customizer plugin version 1.0 Alpha :)
	$('#myImages').myImages({
		wrapper: '<div class="img-item"></div>',
		event: 'mouseover',
		duration: '2000',
		easing: 'swing'
	});
});