;(function($) {
	$.fn.myImages = function(options) {
		var options = options || {};
		var settings = $.extend({
			wrapper: '',
			event: 'click',
			duration: '1000',
			easing: 'easeOutBounce'
		}, options);

		// main object that triggered the plugin
		var self = this;

		// wrapping images
		$('img', self).wrap(settings.wrapper);
		var getClass = $(settings.wrapper).attr('class');

		// setting styles to the wrapper
		$('.' + getClass).css({  // '+' - это конкатенация
			position: 'relative'
		});

		$('.' + getClass + ' > img').css({
			position: 'relative'
		});

		// going through each wrapped object and change title from image into separate div
		$.each($('.' + getClass), function() {
			var img = this.querySelector('img'),
				title = $(img).attr('title');
				$(img).removeAttr('title');
			$(this).append($('<div></div>', {
				html: title,
				style: "opacity:0; display: inline-block; vertical-align: top; max-width: 700px"
			}));
		});

		// pointing on each DOM node
		$(self).on(settings.event, '.' + getClass, function(e) {

			if (!$(this).hasClass('movedOut')) {
				$(self).find('.' + getClass).removeClass(function () {
					$(this).hasClass('movedOut') && jumpIn(this);
					return 'movedOut';
				});
				$(this).addClass('movedOut');
				$(this).hasClass('movedOut') && jumpOut(this);
			} else {
				$(this).removeClass(function () {
					$(this).hasClass('movedOut') && jumpIn(this);
					return 'movedOut';
				});
			}
		});

		function jumpOut(elem) {
			$(elem).animate({
					"left": "+=50"
				},
				settings.duration, 
				settings.easing, 
				function() {
					var div_tag = elem.querySelector('div');
					$(div_tag).animate({
							"opacity": 1
						},
						"200"
					);
				}
			);
		}

		function jumpIn(elem) {
			$(elem).animate({
					"left": "-=50"
				},
				"200",
				settings.easing,
				function() {
					var div_tag = elem.querySelector('div');
					$(div_tag).animate({
							"opacity": 0
						},
						"200"
					);
				}
			);
		}
	};
})(jQuery);