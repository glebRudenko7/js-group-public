$(document).ready(function() {

	// setting styles with jQuery .css() method
	// var styles = {
	// 	"color": "red",
	// 	"background-color": "blue"
	// };

	// $('#content > p b').css({
	// 	"color": "red",
	// 	"background-color": "blue"
	// });

	// var color = $('#content > p b').css('color');

	// $('#content p a[href^="http"]').css('font-size', '24px');

	// //jQuery element creation
	// var p_elem = $('<p></p>', {
	// 	html: "<span>test sdfsfs df sfsd fsd</span>",
	// 	style: "color: red; font-weight: 600"
	// });

	// // jQuery add DOM element before other element, there is similar method .after()
	// $('#lists').before(p_elem);

	// // jQuery classes 
	// $('#content').addClass('testMe');

	// $('#content').removeClass('testMe');

	// // jQuery events
	// $('#content').click(function(event) {
	// 	$('#content').toggleClass('testMe');
	// });

	// $('#lists .nav > li').eq(0).find('button').click(function(event) {
	// 	$('#content').hide(400, 'easeInElastic').delay(1000).show(1000, 'swing');
	// });


	// Image customizer plugin version 1.0 Alpha :)
	$('#myImages').myImages({
		wrapper: '<div class="img-item"></div>',
		event: 'click',
		duration: '2000',
		easing: 'swing'
	});

$.ajax({
	url: 'http://jsonplaceholder.typicode.com/posts',
	method: 'GET',
	beforeSend: function() {
		// alert('123');
	}
	}).then(function(data, xhr) {
		console.log(data);
		setTimeout(function() {
			$.each(data, function(index) {
				if (index == 10) return false;
				var elem = $('<div></div>', {
					html: "<h2>" + this.title + "</h2><p>" + this.body + "</p>",
					class: "item"
				});
				$('#container').append(elem);
			});
		}, 3000);
	}, function(xhr) {});
});