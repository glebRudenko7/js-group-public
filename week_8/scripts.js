$(document).ready(function() {
	var root = 'http://jsonplaceholder.typicode.com';
	$.ajax({
	  url: root + '/posts',
	  method: 'GET',
	  beforeSend: function() {
	  	//alert('123');
	  }
	}).then(function(data, xhr) {
		console.log(data);
	  $.each(data, function(index) {

	  	if (index == 10) return false;

	  	var elem = $('<div></div>', {
	  		html: "<h2>" + this.title + "</h2><p>" + this.body + "</p>",
	  		class: "item"
	  	});
	  	$('#content').append(elem);
	  });
	}, function(xhr) {});
});