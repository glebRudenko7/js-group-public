var ctrl = angular.module('myApp.controllers', []);

ctrl.controller('searchCtrl', ['$scope', 'search', 'searchPromise', function($scope, search, searchPromise) {
	$scope.searchQ = '';
	$scope.people = searchPromise || [];
	$scope.sbmForm = function() {
		search.getResults($scope.searchQ).then(function(res) {
			$scope.people = res;
		});
	};
}]);