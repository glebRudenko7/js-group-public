(function() {
	var app = angular.module('myApp', ['ui.router', 'myApp.controllers', 'myApp.services']);

	app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$stateProvider.state('home', {
			url: "/home",
			templateUrl: 'angular-app/views/home.html'
		});

		$stateProvider.state('search', {
			url: "/search",
			templateUrl: 'angular-app/views/search.html',
			controller: 'searchCtrl',
			resolve: {
				searchPromise: function($stateParams, search) {
					return search.getResults('');
				}
			}
		});

		$urlRouterProvider.otherwise('home');
	}]);

})();