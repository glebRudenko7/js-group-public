var srv = angular.module('myApp.services', []);

srv.factory('search', ['$http', function($http) {
	var obj = {};

	obj.getResults = function(query) {
		var queryParams = {
			params: {
				searchQ: query
			}
		};
		return $http.get('/search', queryParams).then(function(data) {
			return data.data;
		});
	};

	return obj;
}]);