var express = require('express');
var router = express.Router();

var data = [
  {
    "index": 0,
    "guid": "76e888fc-7a42-4b90-a2f4-fd697a1f750c",
    "isActive": false,
    "balance": "$2,217.42",
    "picture": "http://placehold.it/32x32",
    "age": 30,
    "eyeColor": "green",
    "name": "Shelly Stephens",
    "gender": "female",
    "company": "SONGLINES",
    "email": "shellystephens@songlines.com",
    "phone": "+1 (901) 482-2396",
    "address": "124 Veronica Place, Thornport, Maine, 6669",
    "about": "In aliquip sit tempor minim eu ex adipisicing ipsum. Labore sint ipsum reprehenderit consequat proident sint qui ullamco ex do ipsum dolor. Cupidatat nostrud adipisicing duis in nulla occaecat cillum reprehenderit esse amet sit elit tempor irure. Magna minim eu fugiat dolore magna in enim incididunt commodo ullamco elit eu. Id eiusmod do ullamco ut ad ullamco laborum aute laboris aliquip culpa esse labore et. Velit non ea occaecat dolore non qui amet elit.\r\n",
    "registered": "2015-10-26T07:07:14 -02:00",
    "tags": [
      "duis",
      "enim",
      "non",
      "ad",
      "consectetur",
      "proident",
      "quis"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Claudia Fuller"
      },
      {
        "id": 1,
        "name": "Arline Perkins"
      },
      {
        "id": 2,
        "name": "Shanna Day"
      }
    ]
  },
  {
    "index": 1,
    "guid": "79be12a0-f976-4242-8cfc-2e4ea535a644",
    "isActive": true,
    "balance": "$2,219.27",
    "picture": "http://placehold.it/32x32",
    "age": 21,
    "eyeColor": "brown",
    "name": "Margret Dejesus",
    "gender": "female",
    "company": "UBERLUX",
    "email": "margretdejesus@uberlux.com",
    "phone": "+1 (916) 484-2941",
    "address": "985 Cook Street, Toftrees, Maryland, 3295",
    "about": "Eiusmod laboris nisi ea esse tempor labore excepteur ipsum proident. Cillum cillum nisi culpa deserunt esse incididunt non ut ex consectetur aliquip voluptate quis proident. In sit deserunt anim adipisicing anim ut elit excepteur. Consequat dolor reprehenderit sint qui mollit nulla nostrud.\r\n",
    "registered": "2015-08-19T02:50:29 -03:00",
    "tags": [
      "est",
      "ex",
      "reprehenderit",
      "enim",
      "esse",
      "ipsum",
      "non"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Cain Long"
      },
      {
        "id": 1,
        "name": "Ayala Munoz"
      },
      {
        "id": 2,
        "name": "Shelton Brennan"
      }
    ]
  },
  {
    "index": 2,
    "guid": "f065f237-7e3f-4479-9d73-ec354499b373",
    "isActive": false,
    "balance": "$1,128.75",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "blue",
    "name": "Robinson Bolton",
    "gender": "male",
    "company": "QUARMONY",
    "email": "robinsonbolton@quarmony.com",
    "phone": "+1 (814) 565-3775",
    "address": "168 Nassau Street, Finderne, Minnesota, 3249",
    "about": "Voluptate laborum amet tempor eu aliquip anim voluptate sit aute aliqua non est exercitation. Aute cillum tempor officia aliqua ullamco culpa fugiat velit cupidatat laboris pariatur do ad occaecat. Amet do esse do aliqua eiusmod magna eiusmod tempor laborum reprehenderit eiusmod. Proident officia in ullamco ullamco. Labore nisi amet laboris deserunt aute deserunt sit consequat irure commodo fugiat.\r\n",
    "registered": "2016-03-18T03:50:59 -02:00",
    "tags": [
      "nulla",
      "duis",
      "incididunt",
      "consectetur",
      "dolore",
      "excepteur",
      "labore"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Grimes Wheeler"
      },
      {
        "id": 1,
        "name": "Pratt Fischer"
      },
      {
        "id": 2,
        "name": "Beck Conley"
      }
    ]
  },
  {
    "index": 3,
    "guid": "ca203b48-de02-430f-b6b0-c46b350cab82",
    "isActive": false,
    "balance": "$1,817.97",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "green",
    "name": "Vicky Fernandez",
    "gender": "female",
    "company": "OCTOCORE",
    "email": "vickyfernandez@octocore.com",
    "phone": "+1 (976) 578-2003",
    "address": "280 Macon Street, Mathews, South Dakota, 8433",
    "about": "Excepteur laborum reprehenderit aliqua nulla ea excepteur in est aliqua commodo. In in nisi officia anim deserunt est veniam officia proident nostrud aliquip excepteur elit amet. Nisi enim laboris velit occaecat commodo consectetur incididunt cupidatat mollit amet dolor. Voluptate nulla tempor aliquip aute magna esse eiusmod Lorem sit. Cupidatat eiusmod proident amet in consequat.\r\n",
    "registered": "2014-12-22T02:08:50 -02:00",
    "tags": [
      "excepteur",
      "sunt",
      "cillum",
      "ipsum",
      "qui",
      "sint",
      "eiusmod"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Kaitlin Ayers"
      },
      {
        "id": 1,
        "name": "Doyle Watson"
      },
      {
        "id": 2,
        "name": "Zamora Schneider"
      }
    ]
  },
  {
    "index": 4,
    "guid": "c90153e0-5312-4076-82b8-92cbfe344ba4",
    "isActive": true,
    "balance": "$1,515.27",
    "picture": "http://placehold.it/32x32",
    "age": 33,
    "eyeColor": "green",
    "name": "Dorothy Roy",
    "gender": "female",
    "company": "ZUVY",
    "email": "dorothyroy@zuvy.com",
    "phone": "+1 (896) 486-3269",
    "address": "521 Newkirk Avenue, Topanga, Colorado, 9521",
    "about": "Pariatur duis cillum exercitation exercitation cillum ex nostrud est deserunt qui elit aliquip. Fugiat culpa occaecat enim labore. Proident magna est anim non laboris eiusmod ut sunt incididunt sunt occaecat. Minim fugiat non esse elit aliquip elit ad aliquip incididunt commodo. Non dolor amet dolor sint non. Sint quis Lorem commodo laborum deserunt enim cupidatat sunt commodo.\r\n",
    "registered": "2015-03-14T01:15:57 -02:00",
    "tags": [
      "quis",
      "officia",
      "ea",
      "sint",
      "ea",
      "aute",
      "ullamco"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Shannon Wilder"
      },
      {
        "id": 1,
        "name": "Nelda Dominguez"
      },
      {
        "id": 2,
        "name": "Winters Joseph"
      }
    ]
  },
  {
    "index": 5,
    "guid": "32bf4082-73b0-420e-a606-0fec22cb8cfc",
    "isActive": false,
    "balance": "$3,997.74",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "brown",
    "name": "Viola Holman",
    "gender": "female",
    "company": "IDEALIS",
    "email": "violaholman@idealis.com",
    "phone": "+1 (970) 587-2376",
    "address": "243 Duffield Street, Marbury, North Carolina, 4818",
    "about": "Excepteur dolore enim voluptate consequat labore laboris mollit ex quis nisi. Dolor enim consectetur amet mollit. Excepteur reprehenderit ipsum irure dolore labore irure id reprehenderit commodo dolor consectetur ex. Voluptate eiusmod nisi cupidatat cupidatat laborum Lorem ex laborum consequat aliqua ut laboris. Amet consectetur magna occaecat esse eu sint eu ea.\r\n",
    "registered": "2015-08-22T09:27:34 -03:00",
    "tags": [
      "pariatur",
      "velit",
      "ipsum",
      "in",
      "cupidatat",
      "pariatur",
      "sunt"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Lois Hess"
      },
      {
        "id": 1,
        "name": "Reid Douglas"
      },
      {
        "id": 2,
        "name": "Benton Ray"
      }
    ]
  },
  {
    "index": 6,
    "guid": "19e262f4-9421-422a-9900-57fc87a39e1b",
    "isActive": true,
    "balance": "$3,829.94",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "brown",
    "name": "Banks Henson",
    "gender": "male",
    "company": "CONFRENZY",
    "email": "bankshenson@confrenzy.com",
    "phone": "+1 (910) 475-2964",
    "address": "170 Clermont Avenue, Camptown, Nebraska, 5049",
    "about": "Dolor ullamco sit ut aute sint mollit irure exercitation. Exercitation ut aliqua ea eu. Proident laboris duis eu nostrud consectetur et quis. Sunt sit ipsum veniam Lorem minim est enim amet voluptate esse. Aliqua consequat voluptate sunt nostrud aute. Cupidatat in laboris enim exercitation labore mollit ullamco veniam duis exercitation laborum tempor.\r\n",
    "registered": "2016-01-20T05:28:06 -02:00",
    "tags": [
      "Lorem",
      "deserunt",
      "dolor",
      "ut",
      "do",
      "ut",
      "quis"
    ],
    "friends": [
      {
        "id": 0,
        "name": "John Webb"
      },
      {
        "id": 1,
        "name": "Jeannine Lynch"
      },
      {
        "id": 2,
        "name": "Eliza Galloway"
      }
    ]
  },
  {
    "index": 7,
    "guid": "410be01d-d3b6-4ac0-a3ed-f8ebc0b9a296",
    "isActive": false,
    "balance": "$1,387.58",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "eyeColor": "green",
    "name": "Cathleen Norris",
    "gender": "female",
    "company": "KAGE",
    "email": "cathleennorris@kage.com",
    "phone": "+1 (894) 411-2979",
    "address": "513 Aurelia Court, Corinne, Puerto Rico, 5059",
    "about": "Consequat voluptate mollit sit aliquip officia voluptate commodo elit sint reprehenderit. Quis sit occaecat excepteur ex in laborum minim cupidatat ex minim exercitation qui ullamco esse. Do do eiusmod amet amet dolor commodo excepteur laborum tempor adipisicing culpa amet sunt amet. Fugiat officia cupidatat et ex ex dolor consectetur. Cillum veniam enim aliqua eiusmod dolor irure fugiat irure et eu occaecat duis ad velit.\r\n",
    "registered": "2015-08-07T03:42:56 -03:00",
    "tags": [
      "nulla",
      "pariatur",
      "eu",
      "ut",
      "magna",
      "proident",
      "commodo"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Leach Ramsey"
      },
      {
        "id": 1,
        "name": "Joanna Curry"
      },
      {
        "id": 2,
        "name": "Lorie Hyde"
      }
    ]
  },
  {
    "index": 8,
    "guid": "9721ea91-efeb-465a-9749-faba9634e1c9",
    "isActive": true,
    "balance": "$2,575.40",
    "picture": "http://placehold.it/32x32",
    "age": 32,
    "eyeColor": "brown",
    "name": "Dalton Byers",
    "gender": "male",
    "company": "PLAYCE",
    "email": "daltonbyers@playce.com",
    "phone": "+1 (879) 532-3729",
    "address": "185 Townsend Street, Whitestone, New Mexico, 1203",
    "about": "Eu officia voluptate ea reprehenderit occaecat consequat occaecat mollit aliquip. Aliquip deserunt anim velit nostrud eiusmod Lorem ex pariatur consectetur do. Adipisicing id tempor consequat labore veniam incididunt officia in ipsum esse minim exercitation veniam in. Ut eiusmod Lorem aute sunt velit ex nostrud esse proident aliquip aliqua cillum officia nulla. Cupidatat cupidatat eiusmod aute fugiat duis elit proident duis. Laboris sint velit deserunt aliquip amet id consectetur commodo qui pariatur.\r\n",
    "registered": "2015-07-15T10:20:38 -03:00",
    "tags": [
      "quis",
      "do",
      "elit",
      "consectetur",
      "voluptate",
      "sint",
      "excepteur"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Geneva Moss"
      },
      {
        "id": 1,
        "name": "Justice Bennett"
      },
      {
        "id": 2,
        "name": "Kitty Henry"
      }
    ]
  },
  {
    "index": 9,
    "guid": "c7b1e17e-798d-473c-9de3-c58071c8f1b6",
    "isActive": true,
    "balance": "$1,273.82",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "blue",
    "name": "Claire Sears",
    "gender": "female",
    "company": "QUALITERN",
    "email": "clairesears@qualitern.com",
    "phone": "+1 (812) 473-3030",
    "address": "678 Melrose Street, Herald, Guam, 8806",
    "about": "Dolor voluptate labore sunt dolor nostrud sit proident aliquip amet sunt fugiat do ad. Ullamco ad proident consequat in. Veniam ea eu occaecat aliquip exercitation nisi aliquip adipisicing ut commodo aute Lorem duis sit. Cupidatat Lorem adipisicing quis pariatur proident labore exercitation sit incididunt sunt. Reprehenderit eu anim aute laboris reprehenderit enim aliqua enim.\r\n",
    "registered": "2016-04-22T07:04:18 -03:00",
    "tags": [
      "eiusmod",
      "sit",
      "magna",
      "enim",
      "ea",
      "sunt",
      "labore"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Priscilla Aguirre"
      },
      {
        "id": 1,
        "name": "Zimmerman Key"
      },
      {
        "id": 2,
        "name": "Kay Glass"
      }
    ]
  },
  {
    "index": 10,
    "guid": "d2d5636a-e093-456f-9794-7639ddef231f",
    "isActive": false,
    "balance": "$3,291.46",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "green",
    "name": "Shaw Phelps",
    "gender": "male",
    "company": "RODEMCO",
    "email": "shawphelps@rodemco.com",
    "phone": "+1 (973) 546-2375",
    "address": "557 Hanson Place, Gilmore, Northern Mariana Islands, 500",
    "about": "Labore voluptate id aliqua sint aliquip do do adipisicing id sit. Officia occaecat do id ipsum exercitation magna exercitation dolor sint proident dolore elit ad ad. Do quis tempor tempor culpa elit eiusmod amet ad est aliqua aliquip est proident non. Sunt excepteur culpa sunt labore culpa sit amet ut laboris aliquip.\r\n",
    "registered": "2014-06-19T03:39:51 -03:00",
    "tags": [
      "esse",
      "dolore",
      "tempor",
      "qui",
      "mollit",
      "proident",
      "excepteur"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Harriett Holcomb"
      },
      {
        "id": 1,
        "name": "William Mckinney"
      },
      {
        "id": 2,
        "name": "Lavonne Finch"
      }
    ]
  },
  {
    "index": 11,
    "guid": "84af6db1-33ef-4990-8663-75097eeb2bea",
    "isActive": true,
    "balance": "$2,872.15",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "eyeColor": "blue",
    "name": "Lang Reese",
    "gender": "male",
    "company": "JAMNATION",
    "email": "langreese@jamnation.com",
    "phone": "+1 (944) 496-3522",
    "address": "468 Seacoast Terrace, Bellamy, Tennessee, 6581",
    "about": "Lorem esse adipisicing laborum nisi occaecat esse laboris esse anim non commodo laborum qui Lorem. Elit ullamco ea laborum minim consectetur laborum. Dolor veniam et irure pariatur est pariatur anim consequat laborum est quis Lorem. Est eu aliqua voluptate aliqua exercitation duis commodo ex magna. Eiusmod id ipsum ad nulla cupidatat cupidatat sint labore. Aute cillum duis exercitation enim eu cupidatat eu exercitation labore.\r\n",
    "registered": "2014-12-10T06:01:58 -02:00",
    "tags": [
      "Lorem",
      "esse",
      "aliqua",
      "mollit",
      "consequat",
      "dolore",
      "officia"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Oliver Farley"
      },
      {
        "id": 1,
        "name": "Petty Estrada"
      },
      {
        "id": 2,
        "name": "Florence Leon"
      }
    ]
  },
  {
    "index": 12,
    "guid": "0fae4c61-dfc6-4542-9504-fe9d2b5efab8",
    "isActive": false,
    "balance": "$2,129.28",
    "picture": "http://placehold.it/32x32",
    "age": 20,
    "eyeColor": "green",
    "name": "Carpenter Stewart",
    "gender": "male",
    "company": "VORATAK",
    "email": "carpenterstewart@voratak.com",
    "phone": "+1 (934) 587-3024",
    "address": "540 Poplar Avenue, Gratton, Alaska, 3720",
    "about": "Enim nostrud pariatur quis ullamco occaecat occaecat anim commodo elit. In dolore eiusmod excepteur nulla ut irure ea consectetur nisi pariatur. Laborum anim laborum eu quis. Sunt consequat elit et deserunt cupidatat qui in. Officia laborum reprehenderit adipisicing elit exercitation est deserunt excepteur nostrud pariatur excepteur cillum.\r\n",
    "registered": "2014-02-10T06:09:52 -02:00",
    "tags": [
      "officia",
      "in",
      "proident",
      "amet",
      "eu",
      "incididunt",
      "non"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Wiggins Lindsay"
      },
      {
        "id": 1,
        "name": "Mclean Carver"
      },
      {
        "id": 2,
        "name": "Miles Goff"
      }
    ]
  },
  {
    "index": 13,
    "guid": "7627eaf6-6aad-4e28-b8fc-34fe7cae53cb",
    "isActive": true,
    "balance": "$1,665.77",
    "picture": "http://placehold.it/32x32",
    "age": 28,
    "eyeColor": "brown",
    "name": "Curtis Craft",
    "gender": "male",
    "company": "GLUID",
    "email": "curtiscraft@gluid.com",
    "phone": "+1 (905) 557-2716",
    "address": "795 Wythe Place, Westboro, Arkansas, 5562",
    "about": "In velit nisi do laborum ad non deserunt et dolore ad laborum non ut. Laboris cupidatat nostrud nostrud mollit exercitation ad consectetur. Ea occaecat aliqua Lorem amet ea enim pariatur amet quis labore. Lorem amet exercitation aliqua irure minim non cillum tempor non dolor. Eu ea reprehenderit ea elit ut culpa proident anim. Quis ad nisi ipsum tempor fugiat cillum.\r\n",
    "registered": "2015-10-19T04:00:28 -03:00",
    "tags": [
      "id",
      "velit",
      "duis",
      "do",
      "adipisicing",
      "non",
      "laboris"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Valenzuela Callahan"
      },
      {
        "id": 1,
        "name": "Carson Sargent"
      },
      {
        "id": 2,
        "name": "Sharon Bradford"
      }
    ]
  },
  {
    "index": 14,
    "guid": "70c64aa6-96c0-41ff-8768-a3694d6e108c",
    "isActive": true,
    "balance": "$2,961.68",
    "picture": "http://placehold.it/32x32",
    "age": 39,
    "eyeColor": "brown",
    "name": "Mcleod Baxter",
    "gender": "male",
    "company": "ENTHAZE",
    "email": "mcleodbaxter@enthaze.com",
    "phone": "+1 (996) 431-3388",
    "address": "250 Clarendon Road, Bethpage, American Samoa, 3295",
    "about": "Sunt enim aute velit veniam nostrud sint nostrud qui. Non aliquip eu laborum aute sint qui mollit cupidatat non nulla elit laboris officia consequat. Quis nulla dolore quis qui nulla proident anim consectetur ullamco ea id cupidatat est fugiat. Et enim mollit id id eiusmod cillum aute sit laborum commodo fugiat et. Aliqua commodo ea sit commodo excepteur. Quis excepteur duis ut cillum sunt veniam occaecat excepteur do proident ullamco mollit anim.\r\n",
    "registered": "2015-12-17T11:25:51 -02:00",
    "tags": [
      "nostrud",
      "ipsum",
      "exercitation",
      "incididunt",
      "elit",
      "elit",
      "irure"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Jodi Melendez"
      },
      {
        "id": 1,
        "name": "Dorsey Burnett"
      },
      {
        "id": 2,
        "name": "Daisy Garza"
      }
    ]
  },
  {
    "index": 15,
    "guid": "cf4238fe-6657-4caa-9a7b-378f87710ef7",
    "isActive": true,
    "balance": "$2,690.82",
    "picture": "http://placehold.it/32x32",
    "age": 35,
    "eyeColor": "blue",
    "name": "Jolene Carrillo",
    "gender": "female",
    "company": "FILODYNE",
    "email": "jolenecarrillo@filodyne.com",
    "phone": "+1 (813) 537-3285",
    "address": "126 Madoc Avenue, Eggertsville, Marshall Islands, 1977",
    "about": "Exercitation ipsum eu officia tempor consequat tempor quis dolore tempor fugiat dolore quis incididunt. Sit sit id elit est. Nulla aute laborum deserunt officia nisi nulla Lorem esse ipsum in laboris voluptate magna sunt. Duis ullamco dolor esse minim eu est id duis tempor ut sunt ex qui aliqua. Occaecat labore labore ipsum est ea cupidatat elit adipisicing est. Ipsum culpa Lorem eu dolore sint do quis amet excepteur. Adipisicing proident elit duis reprehenderit aliquip.\r\n",
    "registered": "2014-08-10T03:55:09 -03:00",
    "tags": [
      "ex",
      "dolore",
      "reprehenderit",
      "enim",
      "ullamco",
      "quis",
      "amet"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Wong Townsend"
      },
      {
        "id": 1,
        "name": "Dale Vance"
      },
      {
        "id": 2,
        "name": "Dunlap Spencer"
      }
    ]
  },
  {
    "index": 16,
    "guid": "51a6e3bb-3466-4e65-8504-837e7a05ccc5",
    "isActive": true,
    "balance": "$1,677.87",
    "picture": "http://placehold.it/32x32",
    "age": 34,
    "eyeColor": "green",
    "name": "Rowena Ramirez",
    "gender": "female",
    "company": "WAAB",
    "email": "rowenaramirez@waab.com",
    "phone": "+1 (809) 466-3833",
    "address": "428 Rapelye Street, Gardners, Utah, 7418",
    "about": "Dolor ullamco ut consequat occaecat duis in adipisicing magna minim esse eu enim excepteur. Sunt amet veniam minim est nostrud adipisicing ut qui exercitation in irure laborum irure commodo. Nostrud fugiat elit ea fugiat exercitation minim proident eiusmod mollit ad. Aute veniam nisi culpa aliqua excepteur mollit excepteur elit tempor. Reprehenderit irure aute ullamco eiusmod minim sunt do irure occaecat dolor aliquip.\r\n",
    "registered": "2015-08-25T02:59:12 -03:00",
    "tags": [
      "labore",
      "ea",
      "ex",
      "ipsum",
      "dolore",
      "ipsum",
      "fugiat"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Tammi Slater"
      },
      {
        "id": 1,
        "name": "Lakeisha Shaffer"
      },
      {
        "id": 2,
        "name": "Esmeralda Eaton"
      }
    ]
  },
  {
    "index": 17,
    "guid": "2d144a03-5e0a-4b41-9cec-d7b03ca12d7b",
    "isActive": false,
    "balance": "$3,476.08",
    "picture": "http://placehold.it/32x32",
    "age": 33,
    "eyeColor": "brown",
    "name": "Ramona Pugh",
    "gender": "female",
    "company": "NIPAZ",
    "email": "ramonapugh@nipaz.com",
    "phone": "+1 (826) 597-3425",
    "address": "694 Blake Court, Malott, North Dakota, 7218",
    "about": "Elit cupidatat id elit minim laboris commodo. Mollit sit consectetur deserunt deserunt laborum. Excepteur exercitation nostrud cillum do irure sit veniam id elit occaecat qui cupidatat. Ipsum cupidatat eiusmod officia culpa qui irure dolor non incididunt pariatur. Veniam ullamco aute eu veniam dolor ut.\r\n",
    "registered": "2015-11-08T01:54:57 -02:00",
    "tags": [
      "sunt",
      "in",
      "cupidatat",
      "ea",
      "sunt",
      "exercitation",
      "ex"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Webb Sherman"
      },
      {
        "id": 1,
        "name": "Wendy Kidd"
      },
      {
        "id": 2,
        "name": "Gutierrez Smith"
      }
    ]
  },
  {
    "index": 18,
    "guid": "f4b21e4a-1bf9-4a08-a40f-49dc9f0d0cb0",
    "isActive": true,
    "balance": "$1,177.80",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "blue",
    "name": "Fleming Sheppard",
    "gender": "male",
    "company": "SEALOUD",
    "email": "flemingsheppard@sealoud.com",
    "phone": "+1 (842) 488-2413",
    "address": "721 Hunterfly Place, Homeland, Vermont, 6933",
    "about": "Esse voluptate velit deserunt sunt aliquip occaecat ex cupidatat exercitation. Sint tempor minim anim irure incididunt quis quis ipsum tempor officia elit velit. Pariatur ut cupidatat excepteur velit fugiat ipsum labore ea. Nulla id dolore est consectetur sit dolor ut. Exercitation nulla mollit eu mollit laborum ullamco commodo proident officia minim culpa nostrud.\r\n",
    "registered": "2015-04-05T10:12:09 -03:00",
    "tags": [
      "labore",
      "irure",
      "excepteur",
      "elit",
      "proident",
      "ex",
      "occaecat"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Lupe Petty"
      },
      {
        "id": 1,
        "name": "Horn Velez"
      },
      {
        "id": 2,
        "name": "Walton Caldwell"
      }
    ]
  }
];

router.get('/', function(req, res) {
	var filtered = data.filter(function(item) {
		var query = req.query.searchQ;
		return ~item.name.toLowerCase().indexOf(query.toLowerCase());
	});
	res.json(filtered);
});

module.exports = router;