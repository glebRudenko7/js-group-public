var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express', angTitle: 'Angular' });
});

router.get('/test', function(req, res) {
  res.json({ title: 'Express', angTitle: 'Angular' });
});

module.exports = router;
