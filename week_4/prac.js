//'use strict'
var obj = {checkName:2, me: "Mark"};
//console.log(obj);

/*Object.defineProperty(obj, "checkName", {
	//value: "text",
  //writable: false,
  configurable: true,
  enumerable: true,
  set (value) {
    	this.me = value;
  },
  get () {
  	return this.me;
  }
});

obj.checkName = "Mike";
console.log(obj.checkName);*/


Object.defineProperties(obj, {
	foo: {
    value: "123",
    writable: false,
    configurable: true,
    enumerable: true,
  },
  me: {
  	value: 10,
  }
});
Object.seal(obj);
obj.me = 15;
console.log(obj);

/*var obj = {};
console.dir(obj);

var obj = new Object();
console.dir(obj.toString());*/
var people = {name: "Mike", age: 18};
var obj = Object.create(people, {
	foo: {value: 123},
  bar: {value: 777}
});
//console.dir(obj);
//obj.name = "Sasha";
console.dir(obj);

/*var SomeClass = function() {};

var newObj = new SomeClass();
console.dir(newObj);*/

'use strict'
function Factory() {
	var temp = {};
  temp.sum = function(a, b) {
  	return a + b;
  };
  return temp;
}

var test = Factory();

console.dir(test.sum(4, 8));