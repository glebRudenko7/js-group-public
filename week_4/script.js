/* Call, Apply, Bind */
var obj = {num: 7};
function sum(a, b, c) {
	return this.num + a + b + c;
}

// sum.call(obj, 1, 2, 3);
// sum.apply(obj, [1, 2, 3]);
// var newSum = sum.bind(obj);

// console.log(newSum(4, 3, 2));

/* Closures */

function сounter() {
  var currentCount = 1;

  return function() {
    return currentCount++;
  };
}

var counter1 = сounter();

console.log( counter1() ); // 1
console.log( counter1() ); // 2
console.log( counter1() ); // 3

// var counter2 = сounter();
// console.log( counter2() ); // 1

/* Higher order function*/

function unless(test, then) {
  if (!test) then();
}
function repeat(times, body) {
  for (var i = 0; i < times; i++) body(i);
}

repeat(7, function(n) {
  unless(n % 2, function() {
    console.log(n, "is even");
  });
});
// → 0 is even
// → 2 is even


// Our custom higher order filter function
function filter(array, callback) {
	var passed = [];

	for (var i = 0; i < array.length; i++) {
		callback(array[i], i) && passed.push(array[i]);
	}

	return passed;
}
var myArr = [7, 6, 1, 25, 23, 67, 17, 33]
var filtered = filter(myArr, function(elem, index) {
	return elem > 17 && elem < 33;
});

console.log(filtered);


/* try and catch */

function withError() {
	'use strict'
	var x;
	try {
		a = 3;
		x = 5 - a;
	}
	catch(e) {
		throw e + " thats the error";
	}
	console.log('1234');
}

//console.log(withError());

var re = /кайф,\s(сплющь).+?(вши)/ig;
var result = re.exec('Эх, чужд кайф, сплющь объём вши, грызя цент.');

//console.log(result);

function Class() {
	this.name = 'tset';
	this.surname = 'test1';
	if (!this.test) {
		Class.prototype.test = function () {
			return this.name + this.surname;
		};
	}
}

var hey = new Class();
console.log(hey.test());

