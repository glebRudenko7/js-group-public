import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getUsers, searchUsers} from '../actions/usersActions';

class App extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {search: "123"};
		this.loadUsers = this.loadUsers.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.inputSet = this.inputSet.bind(this);
	}

	loadUsers() {
		this.props.getUsers();
	}

	handleSubmit(event) {
		event.preventDefault();
		this.props.searchUsers(this.state.search);
	}

	inputSet(event) {
		this.setState({search: event.target.value});
	}

	render() {
		return (
			<div>
				<h1>Hello React-Redux</h1>
				<form onSubmit={this.handleSubmit}>
					<input type="text" name="search" value={this.state.search} onChange={this.inputSet}/>	
					<input type="submit" value="Search people"/>
				</form>
				<button onClick={this.loadUsers}>Load People</button>
				{
					this.props.users.map( (user, index) => <div key={index}> {user.name}, {user.email}</div> )
				}
			</div>
		);
	}
}


App = connect(
	(state) => { return {users: state.users} },
	(dispatch) => bindActionCreators({getUsers, searchUsers}, dispatch)
)(App);

export default App;