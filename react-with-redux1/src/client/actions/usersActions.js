import people from '../api/db_sample';

export function getUsers() {
	return {
		type: 'GET_USERS',
		data: people
	}
}

export function searchUsers(query) {
	console.log(query);
	let filtered = people.filter((item) => {
		return ~item.name.indexOf(query);
	});
	return {
		type: 'SEARCH_USERS',
		data: filtered
	}
}