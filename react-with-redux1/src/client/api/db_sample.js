const people = [
  {
    "_id": "5825ffdd57fd3284de80cff3",
    "index": 0,
    "guid": "b3e955f2-f281-4cbd-bad2-4eb8fc45c99c",
    "isActive": false,
    "balance": "$2,143.92",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "brown",
    "name": "Hurley Pratt",
    "gender": "male",
    "company": "COSMETEX",
    "email": "hurleypratt@cosmetex.com",
    "phone": "+1 (958) 470-3588",
    "address": "166 Coffey Street, Coaldale, Arkansas, 5606",
    "about": "Aliquip occaecat non elit anim dolor sit dolore incididunt aute eiusmod. Proident nostrud eiusmod reprehenderit id sunt ea aute veniam quis dolore esse. Eiusmod minim ullamco ad culpa consectetur est magna culpa Lorem sunt ex deserunt. Ea ut et laborum eu id.\r\n",
    "registered": "2014-03-14T09:35:50 -02:00",
    "latitude": 16.152563,
    "longitude": -49.683386,
    "tags": [
      "aliqua",
      "nostrud",
      "sunt",
      "culpa",
      "nulla",
      "minim",
      "reprehenderit"
    ],
    "greeting": "Hello, Hurley Pratt! You have 10 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd855c48b5ce51c9ff",
    "index": 1,
    "guid": "dc6d66e1-407e-4695-9972-4a442e755c7c",
    "isActive": false,
    "balance": "$3,460.16",
    "picture": "http://placehold.it/32x32",
    "age": 27,
    "eyeColor": "blue",
    "name": "Pugh Talley",
    "gender": "male",
    "company": "SONGLINES",
    "email": "pughtalley@songlines.com",
    "phone": "+1 (967) 577-3209",
    "address": "825 Emmons Avenue, Groveville, Alabama, 2088",
    "about": "Mollit minim quis mollit qui dolor nostrud id aliquip duis dolor ea qui. Aliquip dolore eu ut sunt exercitation voluptate cupidatat. Ex ullamco enim ut sint est non ad.\r\n",
    "registered": "2016-08-05T06:04:43 -03:00",
    "latitude": 38.615006,
    "longitude": -130.177439,
    "tags": [
      "deserunt",
      "cillum",
      "duis",
      "excepteur",
      "incididunt",
      "veniam",
      "fugiat"
    ],
    "greeting": "Hello, Pugh Talley! You have 10 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5825ffdd355e3ead5677886e",
    "index": 2,
    "guid": "007a582f-4ee5-4716-ac8a-4b2bcf0aee71",
    "isActive": false,
    "balance": "$1,758.35",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "green",
    "name": "Peck Carter",
    "gender": "male",
    "company": "BRAINQUIL",
    "email": "peckcarter@brainquil.com",
    "phone": "+1 (917) 417-3116",
    "address": "268 Freeman Street, Iola, American Samoa, 651",
    "about": "Proident fugiat enim eiusmod irure ea enim ullamco nisi sit veniam duis elit. Tempor voluptate minim id magna minim do eu reprehenderit adipisicing. Dolore non non anim deserunt esse in aliquip labore ex. Culpa aliqua deserunt consequat culpa esse adipisicing velit nisi ut. Aute ea voluptate qui ex reprehenderit ipsum ex cupidatat eu. Qui cillum velit ullamco eu consectetur eiusmod.\r\n",
    "registered": "2014-06-03T04:24:10 -03:00",
    "latitude": 80.405892,
    "longitude": 64.233762,
    "tags": [
      "ex",
      "eu",
      "voluptate",
      "sit",
      "consequat",
      "ut",
      "adipisicing"
    ],
    "greeting": "Hello, Peck Carter! You have 10 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5825ffdda7921e7cfb2a5954",
    "index": 3,
    "guid": "9c9581f9-7d41-40aa-b3d9-4b85b9ebe0ee",
    "isActive": false,
    "balance": "$3,042.57",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "blue",
    "name": "Murray Bailey",
    "gender": "male",
    "company": "HAIRPORT",
    "email": "murraybailey@hairport.com",
    "phone": "+1 (966) 514-2510",
    "address": "451 Hastings Street, Dotsero, Oklahoma, 6383",
    "about": "Aute id consectetur do aliqua quis cillum aute elit eu ut quis deserunt. Incididunt officia aliqua velit quis magna in. Labore adipisicing aliquip eiusmod sunt dolor duis do est officia aliqua sint ad incididunt exercitation. Dolor ex ullamco commodo veniam sit irure cupidatat. Commodo quis est Lorem amet eu non fugiat nulla veniam laboris.\r\n",
    "registered": "2016-07-07T05:03:35 -03:00",
    "latitude": 13.003055,
    "longitude": -94.787274,
    "tags": [
      "duis",
      "nostrud",
      "laborum",
      "enim",
      "deserunt",
      "cupidatat",
      "ad"
    ],
    "greeting": "Hello, Murray Bailey! You have 6 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5825ffdd048530f0d88af285",
    "index": 4,
    "guid": "3c7579e1-49c0-4642-bcfe-30ea106856b0",
    "isActive": true,
    "balance": "$1,863.92",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "blue",
    "name": "Maxwell King",
    "gender": "male",
    "company": "CEDWARD",
    "email": "maxwellking@cedward.com",
    "phone": "+1 (806) 438-2061",
    "address": "853 Ludlam Place, Bancroft, Maryland, 7426",
    "about": "Commodo do est ullamco ex minim exercitation nisi irure et sit. Fugiat commodo dolor esse incididunt duis ut qui elit ut. Et non officia voluptate veniam minim nisi irure. Cillum id nulla adipisicing cillum anim dolor adipisicing aute quis nulla. Incididunt cillum aliquip adipisicing nisi sunt incididunt reprehenderit. Elit est dolore incididunt id sunt pariatur sit cupidatat.\r\n",
    "registered": "2014-05-22T07:52:15 -03:00",
    "latitude": 18.257874,
    "longitude": 111.458199,
    "tags": [
      "ipsum",
      "labore",
      "exercitation",
      "duis",
      "velit",
      "ullamco",
      "velit"
    ],
    "greeting": "Hello, Maxwell King! You have 7 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd5474aa53f0e7a359",
    "index": 5,
    "guid": "bff6b203-72d3-4ff9-aedc-c6426b9e9601",
    "isActive": false,
    "balance": "$2,034.73",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "blue",
    "name": "Wolf Turner",
    "gender": "male",
    "company": "LIQUIDOC",
    "email": "wolfturner@liquidoc.com",
    "phone": "+1 (974) 443-2882",
    "address": "399 Middleton Street, Bordelonville, New Mexico, 1872",
    "about": "Eiusmod ut laboris commodo reprehenderit reprehenderit. Ea officia proident aliqua eu aliqua consequat elit. Consequat nisi nisi labore elit quis esse do reprehenderit ea culpa laborum. Ea proident Lorem qui Lorem sit sint id dolor excepteur occaecat reprehenderit irure minim qui.\r\n",
    "registered": "2014-12-18T08:03:42 -02:00",
    "latitude": -52.514015,
    "longitude": 57.012977,
    "tags": [
      "consectetur",
      "irure",
      "ex",
      "excepteur",
      "laborum",
      "ullamco",
      "irure"
    ],
    "greeting": "Hello, Wolf Turner! You have 6 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd38a949a26bae4392",
    "index": 6,
    "guid": "cc010348-a140-4ff3-aa70-d07d3f9373f8",
    "isActive": true,
    "balance": "$2,827.55",
    "picture": "http://placehold.it/32x32",
    "age": 26,
    "eyeColor": "brown",
    "name": "Snider Kelley",
    "gender": "male",
    "company": "SINGAVERA",
    "email": "sniderkelley@singavera.com",
    "phone": "+1 (822) 484-2451",
    "address": "947 Lake Street, Morriston, District Of Columbia, 6839",
    "about": "Magna quis aliquip magna dolore deserunt ut ex ut non minim cillum incididunt ex. Eu est elit eu nisi magna aliquip est irure aute do incididunt ipsum enim. Sint reprehenderit fugiat anim commodo ipsum officia non elit sint culpa laboris minim nulla exercitation. Mollit aliquip velit cupidatat proident non nulla magna aliquip occaecat cupidatat tempor anim ut. Velit sint minim aliqua do.\r\n",
    "registered": "2016-05-06T11:38:12 -03:00",
    "latitude": 25.83364,
    "longitude": -165.578764,
    "tags": [
      "proident",
      "nisi",
      "ea",
      "dolore",
      "velit",
      "veniam",
      "dolor"
    ],
    "greeting": "Hello, Snider Kelley! You have 1 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd6e820116bbd36472",
    "index": 7,
    "guid": "5520861c-d68e-449e-8684-7b3ac3526bc5",
    "isActive": true,
    "balance": "$2,584.61",
    "picture": "http://placehold.it/32x32",
    "age": 23,
    "eyeColor": "blue",
    "name": "Virginia Harrell",
    "gender": "female",
    "company": "GEOFORM",
    "email": "virginiaharrell@geoform.com",
    "phone": "+1 (998) 529-3408",
    "address": "136 Lefferts Avenue, Linganore, North Dakota, 3780",
    "about": "Reprehenderit occaecat sit sint minim nulla minim esse id duis aliquip. Aute nostrud adipisicing in Lorem ad sint voluptate nulla et adipisicing. Dolore minim est ex tempor et fugiat deserunt non Lorem dolor sunt. Dolor enim eiusmod aliqua minim adipisicing duis elit anim incididunt. Duis cillum nulla nostrud consectetur eiusmod consequat anim enim occaecat. Incididunt eu velit sint id pariatur ad consequat consectetur duis sunt elit mollit consequat pariatur.\r\n",
    "registered": "2014-03-02T07:28:52 -02:00",
    "latitude": -26.855521,
    "longitude": -16.612689,
    "tags": [
      "laboris",
      "magna",
      "ea",
      "minim",
      "ad",
      "elit",
      "mollit"
    ],
    "greeting": "Hello, Virginia Harrell! You have 1 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5825ffdd2323a27285ab1d00",
    "index": 8,
    "guid": "bf64b1f8-57d1-4d23-a768-68f934dc2667",
    "isActive": false,
    "balance": "$1,699.43",
    "picture": "http://placehold.it/32x32",
    "age": 27,
    "eyeColor": "green",
    "name": "Lowery Petersen",
    "gender": "male",
    "company": "ZORK",
    "email": "lowerypetersen@zork.com",
    "phone": "+1 (850) 588-2868",
    "address": "703 Cooke Court, Maybell, Alaska, 7502",
    "about": "Eu officia eu aliquip excepteur dolor quis ad voluptate deserunt aute excepteur. Fugiat quis eu reprehenderit voluptate mollit et aliquip proident Lorem in. Duis amet ullamco in in non dolore ullamco. Aute eu labore et nulla ullamco deserunt tempor duis quis. Enim dolore laboris fugiat amet occaecat commodo ipsum. Veniam cupidatat duis deserunt irure commodo eiusmod et incididunt velit.\r\n",
    "registered": "2016-06-20T04:56:28 -03:00",
    "latitude": -85.98956,
    "longitude": -166.860176,
    "tags": [
      "commodo",
      "magna",
      "tempor",
      "qui",
      "velit",
      "proident",
      "enim"
    ],
    "greeting": "Hello, Lowery Petersen! You have 4 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5825ffddfa2b7cd439b8cf5f",
    "index": 9,
    "guid": "3cb76e15-b9cd-4282-bebf-397882b59a38",
    "isActive": true,
    "balance": "$1,486.38",
    "picture": "http://placehold.it/32x32",
    "age": 32,
    "eyeColor": "green",
    "name": "Dixie Gates",
    "gender": "female",
    "company": "UNI",
    "email": "dixiegates@uni.com",
    "phone": "+1 (884) 581-2468",
    "address": "291 Chestnut Avenue, Stockdale, North Carolina, 3831",
    "about": "Nostrud sint ipsum voluptate sunt non. Qui culpa aute pariatur eiusmod commodo nostrud ipsum esse cillum do labore deserunt. Consectetur quis consequat elit qui amet minim sit ipsum do cillum. Voluptate cillum Lorem eu do laborum. Ut do sit et esse cupidatat minim laboris non laboris dolore.\r\n",
    "registered": "2015-08-17T07:11:23 -03:00",
    "latitude": 65.220644,
    "longitude": 21.424273,
    "tags": [
      "do",
      "adipisicing",
      "adipisicing",
      "non",
      "non",
      "consectetur",
      "velit"
    ],
    "greeting": "Hello, Dixie Gates! You have 10 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5825ffdde63700f36051c11d",
    "index": 10,
    "guid": "991887c1-0557-4e60-96d7-bf4f28f35500",
    "isActive": false,
    "balance": "$1,633.92",
    "picture": "http://placehold.it/32x32",
    "age": 20,
    "eyeColor": "brown",
    "name": "Battle Griffin",
    "gender": "male",
    "company": "AMRIL",
    "email": "battlegriffin@amril.com",
    "phone": "+1 (990) 591-2483",
    "address": "440 Kensington Walk, Frystown, Guam, 5428",
    "about": "Dolore cupidatat laborum labore proident ullamco voluptate duis. Reprehenderit occaecat deserunt voluptate exercitation enim dolore. Ea nulla ex id velit ea sunt laboris adipisicing ex officia aute ex tempor. Veniam minim consectetur occaecat fugiat laboris irure. Dolore sit laboris sit occaecat ex cillum tempor enim laboris. Minim cillum veniam exercitation sint excepteur tempor.\r\n",
    "registered": "2016-09-21T12:44:51 -03:00",
    "latitude": 81.338698,
    "longitude": -33.33563,
    "tags": [
      "id",
      "incididunt",
      "est",
      "incididunt",
      "nisi",
      "dolor",
      "consequat"
    ],
    "greeting": "Hello, Battle Griffin! You have 6 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5825ffdd1cb1831683c531f4",
    "index": 11,
    "guid": "6f9bef36-c929-4f46-b577-fa21294df057",
    "isActive": false,
    "balance": "$3,276.90",
    "picture": "http://placehold.it/32x32",
    "age": 33,
    "eyeColor": "brown",
    "name": "Traci Blair",
    "gender": "female",
    "company": "HARMONEY",
    "email": "traciblair@harmoney.com",
    "phone": "+1 (942) 411-3734",
    "address": "618 Terrace Place, Elizaville, California, 6871",
    "about": "Ad elit enim enim non adipisicing cupidatat aliqua nulla dolor do et ex. Pariatur irure tempor Lorem sunt quis excepteur. Reprehenderit minim commodo sunt laborum nisi ullamco elit sunt voluptate sit id veniam excepteur. Tempor deserunt pariatur excepteur sit reprehenderit laborum ex sit. Est duis nulla quis dolor do Lorem labore quis quis et. Reprehenderit deserunt et dolore sit ea fugiat do id qui Lorem.\r\n",
    "registered": "2015-10-22T02:02:07 -03:00",
    "latitude": 33.452067,
    "longitude": 28.078333,
    "tags": [
      "voluptate",
      "tempor",
      "pariatur",
      "eu",
      "voluptate",
      "voluptate",
      "enim"
    ],
    "greeting": "Hello, Traci Blair! You have 5 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5825ffddc75fd550a115c59d",
    "index": 12,
    "guid": "44e8d069-a3a1-41f4-b339-b466fe2acd72",
    "isActive": true,
    "balance": "$1,838.21",
    "picture": "http://placehold.it/32x32",
    "age": 31,
    "eyeColor": "blue",
    "name": "Celia Navarro",
    "gender": "female",
    "company": "AUSTEX",
    "email": "celianavarro@austex.com",
    "phone": "+1 (849) 594-3611",
    "address": "548 Nova Court, Windsor, Oregon, 4835",
    "about": "Elit laborum eiusmod est elit nisi magna ad. Minim proident in mollit fugiat proident. Voluptate aute deserunt sint aliqua culpa magna non proident do. Nostrud consequat consequat minim eu veniam magna laborum eiusmod nisi non.\r\n",
    "registered": "2014-04-03T08:35:29 -03:00",
    "latitude": 50.546201,
    "longitude": 131.082147,
    "tags": [
      "consequat",
      "ipsum",
      "exercitation",
      "quis",
      "anim",
      "fugiat",
      "et"
    ],
    "greeting": "Hello, Celia Navarro! You have 5 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5825ffddaaee4cb6a42cabc6",
    "index": 13,
    "guid": "c9766951-0876-433f-8ae9-b7bd48d6e9d8",
    "isActive": true,
    "balance": "$2,492.00",
    "picture": "http://placehold.it/32x32",
    "age": 34,
    "eyeColor": "blue",
    "name": "Randi Leblanc",
    "gender": "female",
    "company": "WAAB",
    "email": "randileblanc@waab.com",
    "phone": "+1 (843) 436-2765",
    "address": "288 Plymouth Street, Stouchsburg, Pennsylvania, 775",
    "about": "Aute est fugiat nostrud cupidatat fugiat est sunt laborum proident. Excepteur irure ut commodo aliquip. Tempor magna sunt eiusmod est mollit ullamco occaecat ut velit do. Anim aliquip ipsum duis quis tempor quis enim elit sint deserunt aliqua dolore incididunt minim. Eu esse mollit aliquip ullamco minim ea deserunt magna culpa sunt ad. Mollit irure officia mollit reprehenderit cillum amet esse. Velit quis duis pariatur ad sunt dolore id proident deserunt irure voluptate aliquip dolore.\r\n",
    "registered": "2015-09-27T10:05:03 -03:00",
    "latitude": -24.460283,
    "longitude": -89.985989,
    "tags": [
      "ullamco",
      "eu",
      "dolore",
      "nisi",
      "et",
      "occaecat",
      "fugiat"
    ],
    "greeting": "Hello, Randi Leblanc! You have 1 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd0eeb77ed9231fa1c",
    "index": 14,
    "guid": "ec1f0fcb-5f69-460d-8588-84138a770668",
    "isActive": false,
    "balance": "$2,476.86",
    "picture": "http://placehold.it/32x32",
    "age": 20,
    "eyeColor": "brown",
    "name": "Mercer Copeland",
    "gender": "male",
    "company": "UNEEQ",
    "email": "mercercopeland@uneeq.com",
    "phone": "+1 (905) 527-3773",
    "address": "281 Front Street, Cuylerville, Florida, 5591",
    "about": "Eiusmod minim quis consequat exercitation ea laborum voluptate elit adipisicing. Lorem excepteur commodo fugiat fugiat incididunt pariatur deserunt occaecat nostrud consequat. Elit pariatur aliqua ipsum amet aliqua proident qui sint. Occaecat incididunt dolore non aute culpa elit sunt. Duis officia culpa elit minim elit aliquip nostrud proident ullamco culpa commodo elit nisi. Sunt in occaecat consectetur tempor elit veniam nulla minim est excepteur est nostrud amet. Ad excepteur duis eiusmod enim nostrud commodo nisi tempor.\r\n",
    "registered": "2015-02-03T05:40:48 -02:00",
    "latitude": 27.338674,
    "longitude": -38.780576,
    "tags": [
      "labore",
      "duis",
      "culpa",
      "voluptate",
      "incididunt",
      "sint",
      "enim"
    ],
    "greeting": "Hello, Mercer Copeland! You have 2 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5825ffddc18b14a70afcdabc",
    "index": 15,
    "guid": "21f484a7-dc4f-41d3-9d11-b8a7803189bb",
    "isActive": false,
    "balance": "$3,516.79",
    "picture": "http://placehold.it/32x32",
    "age": 27,
    "eyeColor": "green",
    "name": "Carver Yates",
    "gender": "male",
    "company": "ENDICIL",
    "email": "carveryates@endicil.com",
    "phone": "+1 (863) 422-3640",
    "address": "647 Harwood Place, Darlington, Wisconsin, 5888",
    "about": "Ea excepteur occaecat anim sint dolor nostrud ullamco cupidatat magna. Fugiat in in mollit sit esse duis sunt qui adipisicing duis dolor sunt id officia. Eiusmod aliqua aliqua fugiat incididunt voluptate proident.\r\n",
    "registered": "2015-10-20T08:07:13 -03:00",
    "latitude": -68.686686,
    "longitude": -74.436109,
    "tags": [
      "ipsum",
      "officia",
      "sit",
      "tempor",
      "consectetur",
      "non",
      "laborum"
    ],
    "greeting": "Hello, Carver Yates! You have 3 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5825ffdd07c864f523c6d170",
    "index": 16,
    "guid": "250dc1e3-9d10-498c-b517-0267c65bc53a",
    "isActive": true,
    "balance": "$2,294.30",
    "picture": "http://placehold.it/32x32",
    "age": 22,
    "eyeColor": "blue",
    "name": "Montgomery Huber",
    "gender": "male",
    "company": "ISOTRACK",
    "email": "montgomeryhuber@isotrack.com",
    "phone": "+1 (850) 578-2442",
    "address": "289 Tampa Court, Blende, Maine, 2003",
    "about": "Consectetur dolor aliqua aliqua proident. Nostrud aliqua fugiat culpa Lorem et excepteur. Laborum incididunt duis culpa non cupidatat est veniam dolor fugiat aliquip ad occaecat in sunt. Et nostrud excepteur ad occaecat irure sunt qui mollit nisi non aute minim qui commodo. Et pariatur incididunt enim et ex ea velit officia incididunt officia aliqua sunt incididunt.\r\n",
    "registered": "2015-10-13T09:24:16 -03:00",
    "latitude": 27.002279,
    "longitude": 153.814985,
    "tags": [
      "dolore",
      "sit",
      "aute",
      "Lorem",
      "velit",
      "et",
      "cillum"
    ],
    "greeting": "Hello, Montgomery Huber! You have 8 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd8887a52131cc53b8",
    "index": 17,
    "guid": "a43dfb08-357a-4e1a-89f2-c914163b501d",
    "isActive": false,
    "balance": "$1,578.67",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "brown",
    "name": "Middleton Clark",
    "gender": "male",
    "company": "GRAINSPOT",
    "email": "middletonclark@grainspot.com",
    "phone": "+1 (950) 536-2923",
    "address": "188 Fulton Street, Vernon, Rhode Island, 9983",
    "about": "Enim velit dolor aliquip nisi est aliqua. Nostrud incididunt minim ipsum tempor nisi. Minim excepteur excepteur qui minim.\r\n",
    "registered": "2016-10-18T06:06:22 -03:00",
    "latitude": 74.20832,
    "longitude": -44.776951,
    "tags": [
      "non",
      "et",
      "ex",
      "aliquip",
      "sint",
      "ex",
      "id"
    ],
    "greeting": "Hello, Middleton Clark! You have 8 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5825ffdd273bdddb72a6fc31",
    "index": 18,
    "guid": "7209c8b2-e86b-4d0b-80c4-802c3092fe91",
    "isActive": false,
    "balance": "$1,958.42",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "eyeColor": "brown",
    "name": "Melinda Ryan",
    "gender": "female",
    "company": "ISOSTREAM",
    "email": "melindaryan@isostream.com",
    "phone": "+1 (944) 577-2121",
    "address": "521 India Street, Ivanhoe, New Hampshire, 9478",
    "about": "Velit culpa labore fugiat culpa consectetur. Commodo non nostrud ut reprehenderit consequat eiusmod adipisicing. In nisi sint laboris cupidatat irure aliqua ut cillum amet est nostrud. Labore voluptate ea nisi et occaecat tempor qui incididunt do sit magna. Ut ut eu veniam do aute. Ut sit culpa elit consectetur proident. Occaecat elit ex minim mollit ad voluptate eiusmod ullamco est ex aliquip qui consequat.\r\n",
    "registered": "2014-01-31T09:41:37 -02:00",
    "latitude": 70.273126,
    "longitude": -122.418706,
    "tags": [
      "ex",
      "consequat",
      "ipsum",
      "qui",
      "ipsum",
      "eiusmod",
      "incididunt"
    ],
    "greeting": "Hello, Melinda Ryan! You have 9 unread messages.",
    "favoriteFruit": "strawberry"
  }
];

export default people;