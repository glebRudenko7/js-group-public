var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
// var BUILD_STYLES_DIR = path.resolve(__dirname, 'src/client/public/styles');
var APP_DIR = path.resolve(__dirname, 'src/client');
// var APP_STYLES_DIR = path.resolve(__dirname, 'src/client/app/styles');

var config = [
  {
    name: 'reactComponents',
    entry: {
      //ContactList: APP_DIR + '/ContactList.jsx',
      simpleClick: APP_DIR + '/index.jsx',

    },
    output: {
      path: BUILD_DIR,
      filename: '[name].bundle.js'
    },
    module : {
      loaders : [
        {
          test : /\.jsx?/,
          include : APP_DIR,
          loader : 'babel'
        }
      ]
    }
  },
  // {
  //   entry: {
  //     styles: APP_STYLES_DIR + '/styles.js'
  //   },
  //   output: {
  //     path: BUILD_STYLES_DIR,
  //     filename: '[name].bundle.js'
  //   },
  //   module: {
  //     loaders: [
  //       {
  //         test : /\.js/,
  //         include : APP_STYLES_DIR,
  //         loader : 'babel'
  //       },
  //       {
  //         test: /\.css$/,
  //         include: APP_STYLES_DIR,
  //         loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
  //       }
  //     ]
  //   },
  //   plugins: [
  //     new ExtractTextPlugin('[name].css')
  //   ]
  // }
];

module.exports = config;