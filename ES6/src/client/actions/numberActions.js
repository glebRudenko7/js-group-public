export function increment() {
	return {
		type: 'ADD_ONE',
		payload: 1
	};
}

export function decrement() {
	return {
		type: 'SUBTRACT_ONE',
		payload: -1
	};
}