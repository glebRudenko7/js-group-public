import { applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';

import logger from './logger';

const middleware = applyMiddleware(promiseMiddleware(), logger);

export default middleware;