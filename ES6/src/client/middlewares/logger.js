const logger = (state) => (next) => (action) => {
	console.warn('State type that was run: ' + action.type);
	return next(action);
}

export default logger;