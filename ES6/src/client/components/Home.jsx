import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { increment, decrement } from '../actions/numberActions';
import { getUsers } from '../actions/userActions';

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.props.getUsers();
	}

	incrementHandler() {
		this.props.increment();
	}

	decrementHandler() {
		this.props.decrement();
	}

	componentDidMount() {
		this.props.getUsers();
	}

	render() {
		return (
			<div>
				<h3>{this.props.title + ' ' + this.props.subtitle}</h3>
				<p>Say my name, {this.props.name}</p>
				<p>Guess a number, {this.props.number}</p>
				<button onClick={this.incrementHandler.bind(this)}>Increment the number</button>
				<button onClick={this.decrementHandler.bind(this)}>Decrement the number</button>
				<div>{this.props.users.users.map((item, index) => {
					return <p key={index}>{item.name}, {item.email}</p>
				})}</div>
			</div>

		)
	}
}

Home = connect(
	(state) => { return {number: state.number, name: state.name, users: state.users}},
 	(dispatch) => bindActionCreators({increment, decrement, getUsers}, dispatch)
)(Home);

export default Home;