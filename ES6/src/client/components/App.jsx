import React from 'react';
import Home from './Home.jsx';

const initState = {
	title: "Some title to change",
	subtitle: "some sub title",
	flag: true
};

class App extends React.Component {
	constructor(props) {
		super(props);

		this.handleChange = this.handleChange.bind(this);
		this.state = initState;
	}
	handleChange(e) {
		let obj = {};
		obj[e.target.name] = e.target.value;
		this.setState(obj);
	}

	render() {
		return (
			<div>
				<h1>Hello React</h1>
				<input type="text" name="title" value={this.state.title} onChange={this.handleChange}/>
				<input type="text" name="subtitle" value={this.state.subtitle} onChange={this.handleChange}/>
				<Home title={this.state.title} subtitle={this.state.subtitle}/>
			</div>
		)
	}
}

export default App;