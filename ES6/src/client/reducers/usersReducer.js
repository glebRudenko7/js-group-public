const initState = {
	users: []
};

function usersReducer(state=initState, action) {
	switch(action.type) {
		// case 'GET_USERS':
		// 	return {...state, users: action.payload.data}
		// break;
		case 'GET_USERS_FULFILLED':
			return {...state, users: action.payload.data}
		break;
		default:
			return state;
		break;
	}
}

export default usersReducer;