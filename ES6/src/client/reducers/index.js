import { combineReducers } from 'redux';

import nameReducer from './nameReducer';
import numReducer from './numReducer';
import usersReducer from './usersReducer';

const reducers = combineReducers({
	number: numReducer,
	name: nameReducer,
	users: usersReducer
});

export default reducers;