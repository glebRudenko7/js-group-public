import {createStore} from 'redux';

import reducers from './reducers';
import middleware from './middlewares';

const store = createStore(reducers, middleware);

store.subscribe(() => {
	console.log('store has changed: ', store.getState());
});

// store.dispatch({
// 	type: 'ADD_ONE'
// });

// store.dispatch({
// 	type: 'CHANGE_NAME',
// 	payload: "Vasya"
// });

// store.dispatch({
// 	type: 'ADD',
// 	payload: 7
// });

export default store;