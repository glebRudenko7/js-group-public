/* Objects and constructors */
var customer = {
	name: "Mike",
	speak: function() {
		return "My name is " + this.name;
	},
	address: {
		street: "Some street",
		city: "Some city",
		state: "NY"
	}
};

customer.address.country = "US";

// CONSTRUCTOR FUNCTION
function Person(name, street) {
	this.name = name;
	this.street = street;

	this.info = function() {
		return "My name is" + this.name + "and I live on" + this.street;
	};
}

var mike = new Person('Mike', 'some street');

// console.log((mike instanceof Person));

/* Prototypes */

function Mammal(name) {
	this.name = name;
	this.getInfo = function() {
		return "The mammals name is" + this.name;
	}
}

Mammal.prototype.sound = "Grrr";
Mammal.prototype.makeSound = function() {
	return this.name + " says " + this.sound;
}

var grover = new Mammal("Grover");
// console.log(grover.makeSound());
// console.log(grover.hasOwnProperty("name"));
for (var prop in grover) {
	// console.log(prop + ": " + grover[prop]);
}

/* Prototypes add methods to JS classes like Array */

Array.prototype.inArray = function(value) {
	for (var i = 0; i < this.length; i++) {
		if(this[i] === value) {
			return true;
		}
	}

	return false;
};
var someArr = [1, 2, 3, 7, 10];

// console.log(someArr.inArray(3));

/* Private properties */

function SecretCode() {
	var secretNum = 78;

	this.guessNum = function(num) {
		if (num > secretNum) return "Lower";
		else if (num < secretNum) return "Higher";
		else return "Guessed";
	};
}

var secret = new SecretCode();
// console.log(secret.guessNum(79));

/* Getters and Setters */

// First way
var address = {
	street: "some street",
	city: "some city",
	state: "some state",

	get getAddress() {
		return this.street + ", " + this.city + ", " + this.state;
	},

	set setAddress(theAddress) {
		var parts = theAddress.toString().split(", ");
		this.street = parts[0] || "";
		this.city = parts[1] || "";
		this.state = parts[2] || "";
	}
};

address.setAddress = "test, some City, test7";
// console.log(address.getAddress);

// Second way (deprecated!)

function Coordinates() {
	this.latitude = 0;
	this.longitude = 0;
}

Object.__defineGetter__.call(Coordinates.prototype, "getCoords", function() {
	return "Lat : " + this.latitude + " Long : " + this.longitude;
});

Object.__defineSetter__.call(Coordinates.prototype, "setCoords", function(coords) {
	var parts = coords.toString().split(", ");
	this.latitude = parts[0] || "";
	this.longitude = parts[1] || "";
});

var testCoords = new Coordinates();
testCoords.setCoords = "40.71, 74.00";
// console.log(testCoords.getCoords);

// Third way => DefineProperty

function Point() {
	this.xPos = 0;
	this.yPos = 0;
}

Object.defineProperty(Point.prototype, "pointPos", {
	get: function() {
		return "X : " + this.xPos + " Y : " + this.yPos;
	},
	set: function(thePoint) {
		var parts = thePoint.toString().split(", ");
		this.xPos = parts[0] || "";
		this.yPos = parts[1] || "";
	}
});

var aPoint = new Point();

aPoint.pointPos = "7, 9";
// console.log(aPoint.pointPos);

// Most updated way of Getters and Setters

var Circle = function(radius) {
	this._radius = radius;
}

Circle.prototype = {
	set radius(radius) {
		this._radius = radius;
	},

	get radius() {
		return this._radius;
	},

	get area() {
		return Math.PI * (this._radius * this._radius);
	}
};

var circ = new Circle(10);
circ.radius = 15;
// console.log(circ.radius, circ.area.toFixed(2));

// Inheritence

function Animal() {
	this.name = "Animal";
	this.toString = function() {
		return "my name is " + this.name;
	};
}

function Canine() {
	this.name = "Canine";
}

function Wolf() {
	this.name = "Wolf";
}

Canine.prototype = new Animal();
Wolf.prototype = new Canine();

Canine.prototype.constructor = Canine;
Wolf.prototype.constructor = Wolf;
var arcticWolf = new Wolf();
// for (var prop in arcticWol) {
// 	console.log(prop + " : " + arcticWol[prop]);
// }
// console.log(arcticWolf.toString());

// middle function
function extend(Child, Parent) {
	var Temp = function() {};
	Temp.prototype = Parent.prototype;
	Child.prototype = new Temp();
	Child.prototype.constructor = Child;
}


/* Object.create */

var obj = Object.create(null, {
	test: {value: '123', writable: true, configurable: true},
  test1: {
    configurable: true,
    get: function() {
    	return this.test;
    },
    set: function(value) {
    	console.log('setting value');
      this.test = [];
    }
  }
});
obj.test1 = 'test4';
// console.log(obj);


/* Factory pattern */
var peopleFactory = function(name, age, state) {
	var temp = {};

	temp.age = age;
	temp.name = name;
	temp.state = state;

	temp.printPerson = function () {
		console.log(this.name, this.age, this,state);
	};

	return temp;
};

/* Call, Apply, Bind */
var obj = {num: 7};
function sum(a, b, c) {
	return this.num + a + b + c;
}
var newSum = sum.bind(obj);

// console.log(newSum(4, 3, 2));