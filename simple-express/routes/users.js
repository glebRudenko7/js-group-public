var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');

/* GET users listing. */
router.get('/', function(req, res, next) {
	var name = req.query.name != '' ? req.query : {};
	User.find(name, function(err, data) {
		if (err) res.json(err);

		res.json(data);
	});
});

router.get('/add', function(req, res) {
  res.send('respond with a resource');
});

module.exports = router;
