var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
	name: String,
	company: String,
	gender: String,
	age: Number,
	email: String,

});

mongoose.model('User', userSchema);