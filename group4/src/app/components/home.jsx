import React from 'react';
import {getPosts} from '../actions/get_actions';

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			query: "",
			posts: []
		};
		this.searchQuery = this.searchQuery.bind(this);
	}
	componentDidMount() {
		getPosts().then((data) => {
			this.setState({posts: data.data});
		});
	}
	searchQuery(e) {
		this.setState({query: e.target.value});
	}
	render() {
		let filteredPosts = this.state.posts.filter((item) => {
			return ~item.title.toLowerCase().indexOf(this.state.query.toLowerCase());
		});
		return (
			<div>
				Posts found {filteredPosts.length}
				<div>
					<input value={this.state.query} onChange={this.searchQuery}/>
				</div>
				<ul>
					{
						filteredPosts.map((item, index) => {
							return <li key={item.id}>{item.title}</li>
						})
					}
				</ul>
			</div>
		);
	}
}

export default Home;