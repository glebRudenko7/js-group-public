import React from 'react';
import {
	BrowserRouter as Router,
	Route,
	Link
} from 'react-router-dom';

import Home from './components/home.jsx';
import About from './components/about.jsx';

class App extends React.Component {
	render() {
		return (
			<Router>
				<div>
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/about">About</Link></li>
					</ul>

					<hr/>

					<div id="content">
						<Route path="/" exact component={Home}/>
						<Route path="/about" component={About}/>
					</div>
				</div>
			</Router>
		);
	}
}

export default App;