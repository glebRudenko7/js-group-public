// // Data types
// ​
// var a = 10; // integer
// var text = "test"; // string
// var the_null = null; // null
// var some_object = {}; // object
// var some_undefiend; // undefiend
// var bool_1 = true; // boolean
// var some_array = []; // not type in javascript, it's a class of Array();
// ​
// // data output types
// ​
// document.write(text + a);
// alert(text + a);
// console.log(text + a); // into our console (F12) on most browsers
// console.dir(text + a); // same as above
// ​
// // postfix operators
// ​
// var b = a++;
// var c = a--;
// // prefix operators
// ​
// var b = ++a;
// var c = --a;
// ​
// // IF, ELSE, ELSE IF
// ​
// // string equals integer
// var i = "test"; // => true
// var j = 7; // => false
// if (i > j) {
//   console.log('HI');
// }
// ​
// // integer equals integer
// var i = 10; // => true
// var j = 10; // => false
// if (i >= j) { // !=, >=, <=, >, <
//   console.log('HI ' + i);
// }
// ​
// // types equals types
// var i = {}; // => true
// var j = {}; // => false
// if (i != j) { // OBJECTS ARE NOT EQUAL!!!
//   console.log('HI');
// }
// ​
// // check for types with "typeof"
// var i = "test";
// var j = "true";
// var q = typeof i; // outputs the type of data and converts to "string" type
// var v = typeof j;
// if (q == v) {
//   console.log('1');
// }
// ​
// // console.log(typeof q);
// // console.log(typeof j);
// ​
// // && => AND and || => OR
// var text = "Sasha",
//     text2 = "Misha";
// if ((text == "Misha" || text2 == "Misha") && text == "Sasha1") {
//   console.log('1');
  
// }
// var b = [10, 15, 30];
// // console.log(b[0]);
// // console.log(b[1]);
// // console.log(b[2]);
// console.log(b.length);
// for (var i = 0; i < b.length; i++) {
//   console.log(b[i]);
// }

// // SWITCH
// switch(a) {
// 	case 10:
// 		console.log('its ' + a);
// 	break;
// 	case 15:
// 		console.log('its ' + a);
// 	break;
// 	default:
// 		console.log('its ' + a);
// 	break;
// }

// WHILE, DO...WHILE
var i = 0;
// while (i < 10) {
// 	// console.log(i);
// 	// i++;
// }

// do {
// 	// console.log(i);
// 	i++;
// } while(i < 10);
for (i; i < 10; i++) {
	// for (var j = 0; j < 3; j++) {
	// 	if (j == 2) {
	// 		break;
	// 	}
	// }
	if (i == 5) {
		continue;
	}
	console.log(i);
}