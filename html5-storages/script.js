document.addEventListener("DOMContentLoaded", function() {
  // localStorage.clear();
  // localStorage.setItem('test', 12345);
  // localStorage.test2 = "7777";
  // localStorage['test3'] = "8888";
  // var storage = localStorage.getItem('test');

  var random = {
    s: 'qwerty',
    n: 400,
    b: true,
    a: [14, 25, 'string', true],
    o: {
      s: 'qwerty',
      n: 200
    }
  };

  /* LOCAL STORAGE */
  // localStorage.setItem('localRandom', JSON.stringify(random, null, 2));

  // JSON Stringify:
  // console.log(localStorage.getItem('localRandom'));
  // JSON Parse:
  // console.dir(JSON.parse(localStorage.getItem('localRandom')));


  /* SESSION STORAGE */
  // sessionStorage.setItem('sessionRandom', JSON.stringify(random, null, 2));

  // JSON Stringify:
  // console.log(sessionStorage.getItem('sessionRandom'));
  // JSON Parse:
  // console.dir(JSON.parse(sessionStorage.getItem('sessionRandom')));

  console.warn(':)');
  console.info('??');
  console.error(':(');
});