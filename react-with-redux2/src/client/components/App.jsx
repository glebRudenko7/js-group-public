import React from 'react';
import {Link} from 'react-router';


class App extends React.Component {
	render() {
		return (
			<div>
				<div className="clearfix">
					<ul className="nav navbar-nav clearfix">
						<li><Link to='/'>Search</Link></li>
						<li><Link to='about'>About</Link></li>
						<li><Link to='add-user'>Add user</Link></li>
					</ul>
				</div>
				
				{this.props.children}
			</div>
		);
	}
}

export default App;