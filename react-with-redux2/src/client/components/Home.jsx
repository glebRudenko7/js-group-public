import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import {getUsers, searchUsers} from '../actions/usersActions';

class Home extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {search: ""};
		this.loadUsers = this.loadUsers.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.inputSet = this.inputSet.bind(this);
	}

	componentDidMount() {
		this.props.getUsers();
	}

	loadUsers() {
		this.props.getUsers();
	}

	handleSubmit(event) {
		event.preventDefault();
		this.props.searchUsers({name: this.state.search});
	}

	inputSet(event) {
		this.setState({search: event.target.value});
	}

	render() {

		return (
			<div>
				<h1>Hello React-Redux</h1>
				<form onSubmit={this.handleSubmit}>
					<div className="form-group">
						<input className="form-control" type="text" name="search" value={this.state.search} onChange={this.inputSet}/>
					</div>
					<input className="btn btn-default" type="submit" value="Search people"/>
				</form>
				{/*<button className="btn btn-primary" onClick={this.loadUsers}>Load People</button>*/}
				{
					this.props.users.map( (user, index) => {
						return (
							<div key={index}> {user.name}, {user.email}</div>
						)
					})
				}
			</div>
		);
	}
}


Home = connect(
	(state) => { return {users: state.users} },
	(dispatch) => bindActionCreators({getUsers, searchUsers}, dispatch)
)(Home);

export default Home;