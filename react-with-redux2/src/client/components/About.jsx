import React from 'react';

class About extends React.Component {
	render() {
		return (
			<div>
				<h1>How Redux works?</h1>
				<div className="move-img">
					<img src="./redux.png"/>
				</div>
			</div>
		);
	}
}

export default About;