import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {addUser} from '../actions/usersActions';
const initState ={
			name: '',
			gender: '',
			email: '',
			age: ''
		}
class UserAdd extends React.Component {
	constructor(props) {
		super(props);
		this.state = initState;
	}

	handleInput(e) {
		let inputObj = {};
		inputObj[e.target.name] = e.target.value;
		this.setState(inputObj);
	}

	handleSubmit(e) {
		e.preventDefault();
		this.props.addUser(this.state);
		this.setState(initState);
	}

	render() {
		const success_msg = <div className="alert alert-success">User was added to you Database!</div>
		return (
			<div>
				{this.props.lastAdd.gotData ? success_msg : null}
				<form action='' onSubmit={this.handleSubmit.bind(this)}>
					<div className="form-group">
						<label>Name: </label>
						<input placeholder="Full name" className="form-control" type="text" name="name" value={this.state.name} onChange={this.handleInput.bind(this)}/>
					</div>
					<div className="form-group">
						<label>Gender: </label>
						<input className="form-control" type="text" name="gender" value={this.state.gender} onChange={this.handleInput.bind(this)}/>
					</div>
					<div className="form-group">
						<label>Email: </label>
						<input className="form-control" type="text" name="email" value={this.state.email} onChange={this.handleInput.bind(this)}/>
					</div>
					<div className="form-group">
						<label>Age: </label>
						<input className="form-control" type="text" name="age" value={this.state.age} onChange={this.handleInput.bind(this)}/>
					</div>
					<input className="btn btn-default" type="submit" value="Add User"/>
				</form>
			</div>
		);
	}
}

UserAdd = connect(
	(state) => {return {lastAdd: state.lastAdd}},
	(dispatch) => bindActionCreators({addUser}, dispatch)
)(UserAdd);

export default UserAdd;