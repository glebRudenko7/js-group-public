import {people, get_people, add_user} from '../api/apiRequests';

export function getUsers() {
	return {
		type: 'GET_USERS',
		payload: people()
	}
}

export function searchUsers(query) {
	console.log(query);
	// let filtered = people.filter((item) => {
	// 	return ~item.name.indexOf(query);
	// });
	return {
		type: 'SEARCH_USERS',
		payload: get_people(query)
	}
}

export function addUser(data) {
	return {
		type: 'ADD_USER',
		payload: add_user(data)
	}
}