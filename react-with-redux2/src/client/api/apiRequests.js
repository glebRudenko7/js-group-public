import axios from 'axios';

//export default people;

export function people() {
  return axios.get('http://localhost:3000/users');
};

export function get_people(query) {
  return axios.get('http://localhost:3000/users/', {params: query});
};

export function add_user(data) {
  return axios.post('http://localhost:3000/users/add', data);
}