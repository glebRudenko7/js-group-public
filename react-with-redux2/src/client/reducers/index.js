import {combineReducers} from 'redux';

import usersReducer from './usersReducer';
import addReducer from './addReducer';

export default combineReducers({
	users: usersReducer,
	lastAdd: addReducer
});