const initState = {
	gettingData: false,
	gotData: false,
	addedUser: {}
};

export default function reducer(state=initState, action) {
	//console.log(action);
	switch(action.type) {
		case 'ADD_USER_FULFILLED':
			return {...state, gotData: true, addedUser: action.payload.data};
		break;
		default:
			return state;
		break;
	}

	return state;
}