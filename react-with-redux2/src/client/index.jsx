import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import {Provider} from 'react-redux';
import store from './store';

import App from './components/App.jsx';
import Home from './components/Home.jsx';
import About from './components/About.jsx';
import UserAdd from './components/UserAdd.jsx';

store.subscribe(() => {
	console.log('store has changed!', store.getState());
});

ReactDOM.render(
	<Provider store={store}>
		<Router history={hashHistory}>
			<Route path="/" component={App}>
				<IndexRoute component={Home}/>
				<Route path="/about" component={About}/>
				<Route path="/add-user" component={UserAdd}/>
			</Route>
		</Router>
	</Provider>,
	document.getElementById('root'));