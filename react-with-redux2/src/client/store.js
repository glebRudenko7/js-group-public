import {createStore, applyMiddleware} from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import reducers from './reducers';
import {logger} from './middlewares'

const middleware = applyMiddleware(promiseMiddleware(), logger);

export default createStore(reducers, middleware);