export const logger = (state) => (next) => (action) => {
	console.log('Action fired: ' + action.type + '!!!!');
	next(action);
};