// Array and Functions

/* ARRAYS */

var array = [1, 3, 6, 7, 9, 10];

// Array methods: push(valueN), pop(), unshift(value), shift(); Working with stacks

// push(valueN);  => Add elements to the end of the array.
array.push(7, 6, 8, {obj: [1, 2]});
console.log('====== PUSH ======');
console.log(array);

// pop(); => Get last element from the array and delete it.
var lastElement = array.pop();
console.log('====== POP ======');
console.log(lastElement);
console.log(array);

// unshift(valueN); => Add elements to the start of the array.
array.unshift({obj2: [7, 8]}, 7, 44);
console.log('====== UNSHIFT ======');
console.log(array);

// shift(); => Get first element from the array and delete it.
var firstElement = array.shift();
console.log('====== SHIFT ======');
console.log(firstElement);
console.log(array);

// Array methods: sort([callback]), reverse(), splice(start, deleteCount[, itemN,...]), (string method)split(separator[, limit]), join([separator = ',']);

// sort([compareCallback]); => sort array elements by unicode, converting them to strings if no compareCallback is set.
var sortArr = [1, 2, 3, 4, 5, 6, 7];
sortArr.sort(function(a, b) {
	// if (a > b) {  // FOR STRINGS
	// 	return -1;
	// }
	// if (a < b) {
	// 	return 1;
	// }
	// return 0;
	return b - a;
});
console.log('====== SORT ======');
console.log(sortArr);

//reverse(); => reverses elements in the array
sortArr.reverse();
console.log('====== REVERSE ======');
console.log(sortArr);

//split(separator[, limit]); => splits string into array.
var myString = "dog, cat, parrot, hamster",
	petsArray = myString.split(', ');
console.log('====== SPLIT ======');
console.log(petsArray);

//join([separator = ',']); => joins array into a string.
var myPets = petsArray.join(' ');
console.log('====== JOIN ======');
console.log(myPets);
// splice(start, deleteCount[, itemN,...]); => remove/add elements from array at starting point with deletetion count.
petsArray.splice(2, 1, 'fish');
console.log('====== SPLICE ======');
console.log(petsArray);

// Array methods: filter(callback[, thisArg]), every(callback[, thisArg]), some(callback[, thisArg]); Working with array modifiers(filters).

// filter(callback(elem, index, array)[, thisArg]); => filters elements in the array and returns filtered array.
// *** Doesn't change the array for wich it was called.
var filtered = array.filter(function(elem, index, array) {
	return elem > 9;
});
console.log('====== FILTER ======');
console.log(filtered);

// every(callback(elem, index, array)[, thisArg]); =>  Checks for every element to return true in the array.
// *** Doesn't change the array for wich it was called.
var every = array.every(function(elem, index, array) {
	return elem < 100;
});
console.log('====== EVERY ======');
console.log(every);

// some(callback(elem, index, array)[, thisArg]); =>  Checks for every element to return true in the array, stops on the first TRUE occurence.
// *** Doesn't change the array for wich it was called.
var some = array.some(function(elem, index, array) {
	return elem == 7;
});
console.log('====== SOME ======');
console.log(some);

// Array methods: map(callback[, thisArg]), reduce(callback[, initValue]), reduceRight(callback[, initValue]);

// map(callback(elem, index, array)[, thisArg]); =>  modify each element of the array.
var map = array.map(function(elem, index, array) {
	return elem * 2;
});
console.log('====== MAP ======');
console.log(map);

// reduce(callback(initialValue, nextValue, index, array)[, initValue]); => reduce runs on each array element and and returns one value instead of an array
var reduce = array.reduce(function(initValue, nextValue, index, array) {
	return initValue + nextValue;
});
console.log('====== REDUCE ======');
console.log(reduce);

/* FUNCTIONS */

// Classic call

function myFunc() {
	console.log('Hey!');
}

// Functional Expression
var myExp = function() { // => Ananymous function
	console.log('Hey! from Expression!');
};

//auto called functions
(function() { console.log('Hey, I was auto called!'); })();


// callback functions
function add(a, b, c) {
	return a + b + c;
}

function addNums(callback, nums) {
	return callback(nums[0], nums[1], nums[2]);
}
// console.log(addNums(add, [1, 2, 3]));


function custom(array) {
	var sum = 0,
		arr = array;
	console.log('test');
	for (var i = 0; i < arr.length; i++) {
		sum += arr[i];
	}
	return sum;
}
var y = custom([4, 5, 6, 7, 8]);
custom([4, 5, 6, 7, 8]);
// console.log(y);
// console.log(custom([7, 5, 6, 7, 8]));
// console.log(custom([4, 5, 9, -7, 8]));

var func = function() { return 10; };
// console.log(func);