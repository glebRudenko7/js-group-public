import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { userDetails } from '../actions/usersAction';

class UserDetails extends React.Component {

	componentDidMount() {
		const user_id = this.props.match.params.user_id; 
		this.props.userDetails(user_id);
	}
	render() {
		const user_id = this.props.match.params.user_id; 
		return (
			<div> User details: {this.props.user.name} {this.props.user.email} </div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.user
	}
}

function matchDispatchToProps(dispatch) {
	return bindActionCreators({userDetails: userDetails}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(UserDetails);