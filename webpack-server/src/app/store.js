import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import reducers from './reducers';

const middleware = applyMiddleware(logger());
const store = createStore(reducers, middleware);

export default store;