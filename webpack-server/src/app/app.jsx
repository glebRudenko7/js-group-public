import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
// import { Router } from 'react-router';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import store from './store';
import UserSearch from './containers/userSearch.jsx';
import UserDetails from './containers/userDetails.jsx';

function Custom(props) {
	return (
		<div> Hi! from custom route</div>
	);
}
class App extends React.Component {
	render() {
		return (
			<Router>
				<div>
				    <ul>
				      <li><Link to="/">Home</Link></li>
				      <li><Link to="/custom">Custom Page</Link></li>
				    </ul>

				    <hr/>

					<Route exact path="/" component={UserSearch}/>
					<Route path="/user/:user_id" component={UserDetails}/>
					<Route path="/custom" component={Custom}/>

				</div>
			</Router>
		)
	}
}

ReactDOM.render(
	<Provider store={store}>
		<App/>
	</Provider>
	, document.getElementById('content'));