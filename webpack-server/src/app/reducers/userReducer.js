import data from '../data';

export default function(state=data, action) {
	switch(action.type) {
		case 'CHANGE_NAME':
			return Object.assign({}, state, {name: action.payload});
			break;
		case 'CHANGE_LASTNAME':
			return Object.assign({}, state, {lastname: action.payload});
			break;
	}

	return state;
}
