import React from 'react';
import { Link } from 'react-router-dom';
function UserItem(props) {
  return(
    <li>
      <h3>{props.user.name}</h3>
      <p>{props.user.email}</p>
      <Link to={`/user/${props.user.guid}`}> View User Details</Link>
    </li>
  )
}

export default UserItem;